package ru.t1consulting.vmironova.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskCompleteByIndexResponse extends AbstractTaskResponse {
}
